import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserProvider } from '../user/user';
import { TeamInterface } from '../../app/models/team-interface';

@Injectable()
export class ClubProvider {

  token:any;
  headers = new HttpHeaders();

  constructor(public http: HttpClient, public userProvider:UserProvider) {
    console.log('Hello ClubProvider Provider');
  }

  getEquipo(idEquipo:string){
    return this.http.get<TeamInterface>('http://192.168.1.77:8090/api/v1/club/'+idEquipo, {headers: this.headers});
  }
}
