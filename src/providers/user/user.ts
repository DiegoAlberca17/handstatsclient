import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'access_token';
const USER_DATA = 'user_data';

@Injectable()
export class UserProvider {

  user = null;
  authenticationState = new BehaviorSubject(false);
  headers = new HttpHeaders();

  constructor(private http: HttpClient, private helper: JwtHelperService, private storage: Storage,
    private plt: Platform, private alertController: AlertController) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(token => {
      if (token) {
        let decoded = this.helper.decodeToken(token);
        let isExpired = this.helper.isTokenExpired(token);
 
        if (!isExpired) {
          this.user = decoded;
          this.authenticationState.next(true);
        } else {
          this.storage.remove(TOKEN_KEY);
          this.authenticationState.next(false);
        }
      }
    });
  }

  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
    this.storage.remove(USER_DATA).then(() => {
    });
  }
 
  isAuthenticated() {
    return this.authenticationState.value;
  }

  loginUser(user:string, passwd:string) {
    return this.http.post('http://192.168.1.77:8090/login', {"usuario": user, "password": passwd}, {responseType: 'text'});
  }

  getUser(idUsuario:string){
    return this.http.get('http://192.168.1.77:8090/api/v1/usuario/'+idUsuario, {headers: this.headers});
  }

  showAlert(msg:string) {
    const alert = this.alertController.create({
      title: 'Error',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


}
