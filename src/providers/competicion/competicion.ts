import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CompetitionInterface } from '../../app/models/competition-interface';

@Injectable()
export class CompeticionProvider {

  headers = new HttpHeaders();

  constructor(public http: HttpClient) {
    console.log('Hello CompeticionProvider Provider');
  }

  getCompeticiones(idCompeticion:string){
    return this.http.get<Array<CompetitionInterface>>('http://192.168.1.77:8090/api/v1/competicion/', {headers: this.headers, params: {"club": `${idCompeticion}`}});
  }

  newCompeticion(nombre:string, club:string){
    return this.http.post<CompetitionInterface>('http://192.168.1.77:8090/api/v1/competicion/', {"nombre": `${nombre}`, "club": {"id":`${club}`}}, {headers: this.headers});
  }

  editCompeticion(competicion:CompetitionInterface){
    console.log(competicion);
    return this.http.put<CompetitionInterface>('http://192.168.1.77:8090/api/v1/competicion/'+competicion.id, {"nombre": `${competicion.nombre}`}, {headers: this.headers});
  }

  deleteCompeticion(competicion:string){
    return this.http.delete('http://192.168.1.77:8090/api/v1/competicion/'+competicion, {headers: this.headers});
  }

}
