import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PlayerInterface } from '../../app/models/player-interface';

@Injectable()
export class JugadorProvider {

  headers = new HttpHeaders();

  constructor(public http: HttpClient) {
    console.log('Hello JugadorProvider Provider');
  }

  getJugadores(idEquipo:string){
    return this.http.get<Array<PlayerInterface>>('http://192.168.1.77:8090/api/v1/jugador/', {headers: this.headers, params: {"club": `${idEquipo}`}});
  }

  newJugador(nombre:string, dorsal:string, imagen:string, club:string){
    return this.http.post<PlayerInterface>('http://192.168.1.77:8090/api/v1/jugador', {"nombre": `${nombre}`,"imagen": `${imagen}` , "dorsal": `${dorsal}`, "club": {"id":`${club}`}}, {headers: this.headers});
  }

  editJugador(jugador:PlayerInterface){
    console.log(jugador);
    return this.http.put<PlayerInterface>('http://192.168.1.77:8090/api/v1/jugador/'+jugador.id, {"nombre": `${jugador.nombre}`, "dorsal": `${jugador.dorsal}`}, {headers: this.headers});
  }

  deletePlayer(jugador:string){
    return this.http.delete('http://192.168.1.77:8090/api/v1/jugador/'+jugador, {headers: this.headers});
  }

}
