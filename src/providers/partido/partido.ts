import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatchInterface } from '../../app/models/match-interface';
import { CompetitionInterface } from '../../app/models/competition-interface';

@Injectable()
export class PartidoProvider {

  headers = new HttpHeaders();

  constructor(public http: HttpClient) {
    console.log('Hello PartidoProvider Provider');
  }

  getPartidos(idCompeticion:string){
    return this.http.get<Array<MatchInterface>>('http://192.168.1.77:8090/api/v1/partido/', {headers: this.headers, params: {"competicion": `${idCompeticion}`}});
  }

  newMatch(jornada:number, fecha:string, lugar: string, escudoRival:string, nombreRival:string, competicion:CompetitionInterface){
    return this.http.post<MatchInterface>('http://192.168.1.77:8090/api/v1/partido/', {
      "jornada": `${jornada}`,
      "fecha": `${fecha}`,
      "lugar": `${lugar}`,
      "escudoRival": `${escudoRival}`,
      "nombreRival": `${nombreRival}`,
      "golesFavor": 0,
      "golesContra": 0,
      "finalizado": 0,
      "competicion": {"id":`${competicion.id}`}
    }, {headers: this.headers});
  }

  editMatch(partido:MatchInterface){
    return this.http.put<MatchInterface>('http://192.168.1.77:8090/api/v1/partido/'+partido.id, {}, {headers: this.headers});
  }

  deleteMatch(partido:string){
    return this.http.delete('http://192.168.1.77:8090/api/v1/partido/'+partido, {headers: this.headers});
  }

}
