import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-my-stats',
  templateUrl: 'my-stats.html',
})
export class MyStatsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
