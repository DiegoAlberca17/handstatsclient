import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TeamInterface } from '../../../app/models/team-interface';

const USER_DATA = 'user_data';

@IonicPage()
@Component({
  selector: 'page-my-team',
  templateUrl: 'my-team.html',
})
export class MyTeamPage {

  equipo:TeamInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.getTeam();
  }
  
  getTeam(){
    this.storage.get(USER_DATA).then(data => {
      this.equipo = data.club;
    });
  }

}
