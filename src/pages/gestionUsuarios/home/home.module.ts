import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { SignUpPage } from '../sign-up/sign-up';
import { MainPage } from '../main/main';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    HomePage,
    SignUpPage,
    MainPage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    TranslateModule,
  ],
})
export class HomePageModule {}
