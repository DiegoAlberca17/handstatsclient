import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { MainPage } from '../main/main';
import { UserProvider } from '../../../providers/user/user';
import { AlertController } from 'ionic-angular';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'access_token';
const USER_DATA = 'user_data';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  authenticationState = new BehaviorSubject(false);
  public rutaImg: string = "../../assets/imgs/logo_text.png";

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public userService: UserProvider, public alertCtrl: AlertController, private helper: JwtHelperService, private storage: Storage) {
    this.menuCtrl.enable(false, 'sidemenu');
  }

  public user:string;
  public passwd:string;
  public mantenerSesion:boolean;

  startSession() {
    return this.userService.loginUser(this.user, this.passwd).subscribe(
      (data) => { // Success
        this.storage.set(TOKEN_KEY, data);
        this.authenticationState.next(true);
        let decoded = this.helper.decodeToken(data);
        this.storageUsuario(decoded.sub);
        this.navCtrl.setRoot(MainPage);
      },
      (error) =>{
        if (error.status == 401){
          this.showAlert('Datos incorrectos', "ERROR");
        }
      }
    );
  }

  storageUsuario(id:string){
    this.userService.getUser(id).subscribe(
      (data) => { // Success
        this.storage.set(USER_DATA, data);
      },
      (error) =>{
      }
    );
  }

  showAlert(title:string, subtitle:string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

  toSignUp() {
    this.navCtrl.push(SignUpPage);
  }
}
