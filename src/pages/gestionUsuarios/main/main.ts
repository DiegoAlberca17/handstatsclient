import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ClubProvider } from '../../../providers/club/club';
import { Storage } from '@ionic/storage';
import { TeamInterface } from '../../../app/models/team-interface';
import { PlayersPage } from '../../estadisticasJugadores/players/players';
import { CompetitionsPage } from '../../competiciones/competitions/competitions';

const USER_DATA = 'user_data';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  club: TeamInterface = {
    id: '',
    nombre: '',
    logo:''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public clubProvider: ClubProvider,  private storage: Storage) {
    this.menuCtrl.enable(true, 'sidemenu');
    this.storage.get(USER_DATA).then(data => {
      if (data) {
        clubProvider.getEquipo(data.club.id).subscribe(
          (data) => {
            this.club = data;
          }
        );
      }
    });
  }

  navigate(page:string){
    if (page == 'competitions'){
      this.navCtrl.push(CompetitionsPage);
    } else if (page == 'players') {
      this.navCtrl.push(PlayersPage);
    }
  }
}
