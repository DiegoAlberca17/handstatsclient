import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TeamInterface } from '../../../app/models/team-interface';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { CompeticionProvider } from '../../../providers/competicion/competicion';
import { MatchesPage } from '../../partido/matches/matches';


const USER_DATA = 'user_data';

@IonicPage()
@Component({
  selector: 'page-competitions',
  templateUrl: 'competitions.html',
})
export class CompetitionsPage {
  listaCompeticiones:CompetitionInterface[];
  club:TeamInterface;
 
  constructor(public navCtrl: NavController, public navParams: NavParams, public competicionProvider:CompeticionProvider, public storage: Storage, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true, 'sidemenu');
    this.getCompetitions();
  }

  getCompetitions(){
    this.storage.get(USER_DATA).then(data => {
      if (data) {
        this.club = data.club;
        this.competicionProvider.getCompeticiones(this.club.id).subscribe(
          (data) => {
            this.listaCompeticiones = data;
          }
        );
      }
    });
  }

  showMatches(competicion:CompetitionInterface){
    this.navCtrl.push(MatchesPage, {competicion: competicion});
  }
}
