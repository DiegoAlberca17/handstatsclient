import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { MatchInterface } from '../../../app/models/match-interface';
import { PartidoProvider } from '../../../providers/partido/partido';
import { PlayMatchPage } from '../play-match/play-match';

@IonicPage()
@Component({
  selector: 'page-matches',
  templateUrl: 'matches.html',
})
export class MatchesPage {

  competicion:CompetitionInterface;
  listaPartidos:MatchInterface[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public partidoProvider: PartidoProvider) {
    this.competicion = this.navParams.get('competicion');
    this.getMatches();
  }

  ionViewCanEnter(){
    this.getMatches();
  }
  
  getMatches(){
    this.partidoProvider.getPartidos(this.competicion.id).subscribe(
      (data) => {
        this.listaPartidos = data;
      }
    );
  }

  goToMatch(idPartido:string){
    this.navCtrl.push(PlayMatchPage, {partido: idPartido});
  }

}
