import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-play-match',
  templateUrl: 'play-match.html',
})
export class PlayMatchPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
}
