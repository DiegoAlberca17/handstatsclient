import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayMatchPage } from './play-match';

@NgModule({
  declarations: [
    PlayMatchPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayMatchPage),
  ],
})
export class PlayMatchPageModule {}
