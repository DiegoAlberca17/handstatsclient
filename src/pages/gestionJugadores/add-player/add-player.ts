import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { JugadorProvider } from '../../../providers/jugador/jugador';
import { MyPlayersPage } from '../my-players/my-players';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-add-player',
  templateUrl: 'add-player.html',
})
export class AddPlayerPage {

  nombre:string;
  dorsal:string;
  imagen = "https://ceslava.com/blog/wp-content/uploads/2016/04/mistery-man-gravatar-wordpress-avatar-persona-misteriosa-510x510.png";

  constructor(public navCtrl: NavController, public navParams: NavParams, public jugadorProvider:JugadorProvider, public camera:Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
  }
  
  crearJugador() {
    return this.jugadorProvider.newJugador(this.nombre, this.dorsal, this.imagen, this.navParams.get('club')).subscribe(
      () => {
        this.navCtrl.setRoot(MyPlayersPage);
      }
    );
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 800,
      targetHeight: 800,
      quality: 100,
      sourceType: 0,
      allowEdit: true,
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.imagen = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  takeAPhoto(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 200,
      targetHeight: 200,
      quality: 50,
      sourceType: 1,
      allowEdit: true,
      cameraDirection: 0,
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.imagen = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
