import { Component } from '@angular/core';
import { JugadorProvider } from '../../../providers/jugador/jugador';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyPlayersPage } from '../my-players/my-players';
import { PlayerInterface } from '../../../app/models/player-interface';

@IonicPage()
@Component({
  selector: 'page-edit-player',
  templateUrl: 'edit-player.html',
})
export class EditPlayerPage {

  jugador:PlayerInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public jugadorProvider:JugadorProvider) {
    this.jugador = this.navParams.get('jugador');
  }

  editarJugador() {
    console.log(this.jugador);
    return this.jugadorProvider.editJugador(this.jugador).subscribe(
      () => {
        this.navCtrl.setRoot(MyPlayersPage);
      }
    );
  }

}
