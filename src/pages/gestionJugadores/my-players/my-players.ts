import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { PlayerInterface } from '../../../app/models/player-interface';
import { JugadorProvider } from '../../../providers/jugador/jugador';
import { Storage } from '@ionic/storage';
import { TeamInterface } from '../../../app/models/team-interface';
import { AddPlayerPage } from '../add-player/add-player';
import { EditPlayerPage } from '../edit-player/edit-player';

const USER_DATA = 'user_data';

@IonicPage()
@Component({
  selector: 'page-my-players',
  templateUrl: 'my-players.html',
})
export class MyPlayersPage {

  listaJugadores:PlayerInterface[];
  club:TeamInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public jugadorProvider:JugadorProvider, public storage: Storage, public menuCtrl: MenuController, public alertCtrl:AlertController) {
    this.menuCtrl.enable(true, 'sidemenu');
    this.getPlayers();
  }

  getPlayers(){
    this.storage.get(USER_DATA).then(data => {
      if (data) {
        this.club = data.club;
        this.jugadorProvider.getJugadores(this.club.id).subscribe(
          (data) => {
            this.listaJugadores = data;
          }
        );
      }
    });
  }

  addPlayer(){
    this.navCtrl.push(AddPlayerPage, {club: this.club.id});
  }

  editPlayer(jugador:PlayerInterface){
    this.navCtrl.push(EditPlayerPage, {jugador: jugador});
  }

  deletePlayer(idJugador:string){
    this.presentConfirm(idJugador);
  }

  presentConfirm(idJugador:string) {
    let alert = this.alertCtrl.create({
      title: 'Confirmar eliminación',
      message: '¿Seguro que deseas eliminar este jugador?',
      buttons: [
        {
          text: 'Atrás',
          role: 'cancel',
          handler: () => {
            console.log('Eliminación cancelada');
          }
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.jugadorProvider.deletePlayer(idJugador).subscribe(
              () => {
                console.log('Jugador eliminado');
                this.getPlayers();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

}
