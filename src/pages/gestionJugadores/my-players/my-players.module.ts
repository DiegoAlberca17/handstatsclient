import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPlayersPage } from './my-players';

@NgModule({
  declarations: [
    MyPlayersPage,
  ],
  imports: [
    IonicPageModule.forChild(MyPlayersPage),
  ],
})
export class MyPlayersPageModule {}
