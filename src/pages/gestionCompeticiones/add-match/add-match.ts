import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PartidoProvider } from '../../../providers/partido/partido';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-add-match',
  templateUrl: 'add-match.html',
})
export class AddMatchPage {

  jornada:number;
  fecha:string;
  hora:string;
  lugar:string;
  nombreRival:string;
  escudoRival:string;
  competicion:CompetitionInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public partidoProvider: PartidoProvider, public camera:Camera) {
    this.competicion = this.navParams.get('competicion');
  }

  crearPartido(){
    return this.partidoProvider.newMatch(this.jornada, this.fecha, this.lugar, this.escudoRival, this.nombreRival, this.competicion).subscribe(
      () => {
        this.navCtrl.pop();
      }
    );
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 400,
      targetHeight: 400,
      quality: 100,
      sourceType: 0,
      allowEdit: true,
      cameraDirection: 0,
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.escudoRival = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

}
