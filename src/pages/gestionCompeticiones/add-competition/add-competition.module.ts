import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCompetitionPage } from './add-competition';

@NgModule({
  declarations: [
    AddCompetitionPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCompetitionPage),
  ],
})
export class AddCompetitionPageModule {}
