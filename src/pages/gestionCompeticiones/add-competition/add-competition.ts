import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyCompetitionsPage } from '../my-competitions/my-competitions';
import { CompeticionProvider } from '../../../providers/competicion/competicion';

@IonicPage()
@Component({
  selector: 'page-add-competition',
  templateUrl: 'add-competition.html',
})
export class AddCompetitionPage {

  nombre:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public competicionProvider:CompeticionProvider) {
  }
  
  crearCompeticion() {
    return this.competicionProvider.newCompeticion(this.nombre, this.navParams.get('club')).subscribe(
      () => {
        this.navCtrl.setRoot(MyCompetitionsPage);
      }
    );
  }

}
