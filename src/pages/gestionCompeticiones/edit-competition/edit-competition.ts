import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyCompetitionsPage } from '../my-competitions/my-competitions';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { CompeticionProvider } from '../../../providers/competicion/competicion';

@IonicPage()
@Component({
  selector: 'page-edit-competition',
  templateUrl: 'edit-competition.html',
})
export class EditCompetitionPage {

  competicion:CompetitionInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public competicionProvider:CompeticionProvider) {
    this.competicion = this.navParams.get('competicion');
  }

  editarCompeticion() {
    console.log(this.competicion);
    return this.competicionProvider.editCompeticion(this.competicion).subscribe(
      () => {
        this.navCtrl.setRoot(MyCompetitionsPage);
      }
    );
  }
}
