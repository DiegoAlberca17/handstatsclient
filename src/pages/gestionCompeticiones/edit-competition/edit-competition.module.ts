import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditCompetitionPage } from './edit-competition';

@NgModule({
  declarations: [
    EditCompetitionPage,
  ],
  imports: [
    IonicPageModule.forChild(EditCompetitionPage),
  ],
})
export class EditCompetitionPageModule {}
