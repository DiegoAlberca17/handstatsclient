import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { MatchInterface } from '../../../app/models/match-interface';
import { PartidoProvider } from '../../../providers/partido/partido';
import { AddMatchPage } from '../add-match/add-match';

@IonicPage()
@Component({
  selector: 'page-my-matches',
  templateUrl: 'my-matches.html',
})
export class MyMatchesPage {

  competicion:CompetitionInterface;
  listaPartidos:MatchInterface[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public partidoProvider: PartidoProvider) {
    this.competicion = this.navParams.get('competicion');
    this.getMatches();
  }

  ionViewCanEnter(){
    this.getMatches();
  }
  
  getMatches(){
    this.partidoProvider.getPartidos(this.competicion.id).subscribe(
      (data) => {
        this.listaPartidos = data;
      }
    );
  }

  addMatch(){
    this.navCtrl.push(AddMatchPage, {competicion: this.competicion});
  }

}
