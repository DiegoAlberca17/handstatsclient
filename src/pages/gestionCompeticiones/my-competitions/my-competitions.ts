import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TeamInterface } from '../../../app/models/team-interface';
import { CompetitionInterface } from '../../../app/models/competition-interface';
import { CompeticionProvider } from '../../../providers/competicion/competicion';
import { AddCompetitionPage } from '../add-competition/add-competition';
import { EditCompetitionPage } from '../edit-competition/edit-competition';
import { MyMatchesPage } from '../my-matches/my-matches';

const USER_DATA = 'user_data';

@IonicPage()
@Component({
  selector: 'page-my-competitions',
  templateUrl: 'my-competitions.html',
})
export class MyCompetitionsPage {

  listaCompeticiones:CompetitionInterface[];
  club:TeamInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, public competicionProvider:CompeticionProvider, public storage: Storage, public menuCtrl: MenuController, public alertCtrl:AlertController) {
    this.menuCtrl.enable(true, 'sidemenu');
    this.getCompetitions();
  }

  getCompetitions(){
    this.storage.get(USER_DATA).then(data => {
      if (data) {
        this.club = data.club;
        this.competicionProvider.getCompeticiones(this.club.id).subscribe(
          (data) => {
            this.listaCompeticiones = data;
          }
        );
      }
    });
  }

  addCompetition(){
    this.navCtrl.push(AddCompetitionPage, {club: this.club.id});
  }

  editCompetition(competicion:CompetitionInterface){
    this.navCtrl.push(EditCompetitionPage, {competicion: competicion});
  }

  deleteCompetition(idCompeticion:string){
    this.presentConfirm(idCompeticion);
  }

  showMatches(competicion:CompetitionInterface){
    this.navCtrl.push(MyMatchesPage, {competicion: competicion});
  }

  presentConfirm(idCompeticion:string) {
    let alert = this.alertCtrl.create({
      title: 'Confirmar eliminación',
      message: '¿Seguro que deseas eliminar esta competición?',
      buttons: [
        {
          text: 'Atrás',
          role: 'cancel',
          handler: () => {
            console.log('Eliminación cancelada');
          }
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.competicionProvider.deleteCompeticion(idCompeticion).subscribe(
              () => {
                console.log('Competicion eliminada');
                this.getCompetitions();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }
}
