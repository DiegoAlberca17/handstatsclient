import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCompetitionsPage } from './my-competitions';

@NgModule({
  declarations: [
    MyCompetitionsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCompetitionsPage),
  ],
})
export class MyCompetitionsPageModule {}
