import { Component } from '@angular/core';

@Component({
  selector: 'next-match',
  templateUrl: 'next-match.html'
})
export class NextMatchComponent {

  text: string;
  escudoLocal = "http://balonmano.isquad.es/images/afiliacion_clubs/361/square_62713574693062753671.jpg";
  escudoVisitante = "http://balonmano.isquad.es/images/afiliacion_clubs/2899/square_32736e70636d306c7438.jpg";

  constructor() {
  }

}
