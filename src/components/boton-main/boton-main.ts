import { Component, Input  } from '@angular/core';

@Component({
  selector: 'boton-main',
  templateUrl: 'boton-main.html'
})
export class BotonMainComponent {

  @Input('icon') icon : string;
  @Input('text') text : string;

  constructor() {

  }

}
