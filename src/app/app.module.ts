import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/gestionUsuarios/home/home';
import { MainPage } from '../pages/gestionUsuarios/main/main';
import { MyTeamPage } from '../pages/gestionEquipo/my-team/my-team';
import { PlayersPage } from '../pages/estadisticasJugadores/players/players';
import { SettingsPage } from '../pages/gestionUsuarios/settings/settings';
import { SignUpPage } from '../pages/gestionUsuarios/sign-up/sign-up';
import { MyCompetitionsPage } from '../pages/gestionCompeticiones/my-competitions/my-competitions';
import { MyPlayersPage } from '../pages/gestionJugadores/my-players/my-players';
import { MyStatsPage } from '../pages/estadisticasJugadores/my-stats/my-stats';
import { AddPlayerPage } from '../pages/gestionJugadores/add-player/add-player';
import { EditPlayerPage } from '../pages/gestionJugadores/edit-player/edit-player';
import { EditCompetitionPage } from '../pages/gestionCompeticiones/edit-competition/edit-competition';
import { AddCompetitionPage } from '../pages/gestionCompeticiones/add-competition/add-competition';
import { CompetitionsPage } from '../pages/competiciones/competitions/competitions';
import { MyMatchesPage } from '../pages/gestionCompeticiones/my-matches/my-matches';
import { AddMatchPage } from '../pages/gestionCompeticiones/add-match/add-match';
import { EditMatchPage } from '../pages/gestionCompeticiones/edit-match/edit-match';
import { MatchesPage } from '../pages/partido/matches/matches';
import { PlayMatchPage } from '../pages/partido/play-match/play-match';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';

import { BotonMainComponent } from '../components/boton-main/boton-main';
import { NextMatchComponent } from '../components/next-match/next-match';
import { CompeticionProvider } from '../providers/competicion/competicion';
import { UserProvider } from '../providers/user/user';
import { PartidoProvider } from '../providers/partido/partido';
import { ClubProvider } from '../providers/club/club';
import { JugadorProvider } from '../providers/jugador/jugador';
import { AuthInterceptorProvider } from '../providers/auth-interceptor/auth-interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MainPage,
    MyTeamPage,
    PlayersPage,
    SettingsPage,
    SignUpPage,
    MyStatsPage,
    MyPlayersPage,
    AddPlayerPage,
    EditPlayerPage,
    MyCompetitionsPage,
    EditCompetitionPage,
    AddCompetitionPage,
    CompetitionsPage,
    MyMatchesPage,
    AddMatchPage,
    EditMatchPage,
    BotonMainComponent,
    NextMatchComponent,
    MatchesPage,
    PlayMatchPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage],
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MainPage,
    MyTeamPage,
    PlayersPage,
    SettingsPage,
    SignUpPage,
    AddPlayerPage,
    EditPlayerPage,
    MyStatsPage,
    MyPlayersPage,
    MyCompetitionsPage,
    EditCompetitionPage,
    AddCompetitionPage,
    CompetitionsPage,
    MyMatchesPage,
    AddMatchPage,
    EditMatchPage,
    MatchesPage,
    PlayMatchPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorProvider, multi: true},
    CompeticionProvider,
    UserProvider,
    PartidoProvider,
    ClubProvider,
    JugadorProvider,
    AuthInterceptorProvider,
    Camera,
  ]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('access_token');
    },
    whitelistedDomains: ['localhost:5000']
  }
}