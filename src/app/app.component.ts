import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/gestionUsuarios/home/home';
import { SettingsPage } from '../pages/gestionUsuarios/settings/settings';
import { MainPage } from '../pages/gestionUsuarios/main/main';
import { UserProvider } from '../providers/user/user';
import { MyPlayersPage } from '../pages/gestionJugadores/my-players/my-players';
import { MyTeamPage } from '../pages/gestionEquipo/my-team/my-team';
import { MyCompetitionsPage } from '../pages/gestionCompeticiones/my-competitions/my-competitions';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public translateService: TranslateService, public userProvider: UserProvider) {
    this.initializeApp();

    this.pages = [
      { title: 'Inicio', component: MainPage },
      { title: 'Mi equipo', component: MyTeamPage },
      { title: 'Mis jugadores', component: MyPlayersPage },
      { title: 'Mis competiciones', component: MyCompetitionsPage },
      { title: 'Ajustes', component: SettingsPage },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.translateService.use('es');
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  signOut(){
    this.userProvider.logout;
    this.nav.setRoot(HomePage);
  }
}
