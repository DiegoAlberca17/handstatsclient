import { TeamInterface } from "./team-interface";

export interface CompetitionInterface {
    id: string,
    nombre: string,
    club: TeamInterface
}