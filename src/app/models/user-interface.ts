import { TeamInterface } from "./team-interface";

export interface UserInterface {
    id: string,
    usuario: string,
    passwd?: string,
    email: string,
    nombre: string,
    equipo: TeamInterface
}