export interface PlayerInterface {
    id: string,
    nombre: string,
    dorsal: string,
    img: string
}