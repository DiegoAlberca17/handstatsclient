import { CompetitionInterface } from "./competition-interface";

export interface MatchInterface {
    id: string,
    nombreRival: string,
    escudoRival: string,
    fecha: string,
    hora: string,
    golesContra: number,
    golesFavor: number,
    jornada: number,
    lugar: string,
    competicion: CompetitionInterface,
    finalizado: boolean
}